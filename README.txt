This is another document manager module, which implements a 'file'
node in a similar fashion to the way the forum module in core maintains 
discussion forums.

The aim is to build a simple, user friendly and extensible
document management interface. That means support for file uploads, downloads,
revisions. Integrating that in a user friendly directory structure.

Goals - user friendliness, simplicity, easy mainenance, for both site admins and 
developers, dependence on core Drupal only. Certainty. User interface.

Non-goals - universality. This module is not a panacea. It is not going to solve 
a file upload needs. It is not really creating file nodes - it relies on upload.module.
It is not a file api, and is not going to be. Ever. 

To  install the module, follow the usual procedures - drop it in the appropriate modules directory,
enable it, making sure that taxonomy and upload modules are turned on as well.

At the moment you need to create a 'special' content type for files.

You need to create a taxonomy/category vocabulary to serve as a container for folders.

In the Document management site building section select the file node type and the folders vocabulary.

If you have the permissions to upload files and create taxonomy terms you will be able
to upload files and create folders from the docs UI.

That's all.

If you want to help, there is more information in the usual places on drupal.org and especially in the
file HACKME, which is maintained up-to date, well, kinda, sorta.
